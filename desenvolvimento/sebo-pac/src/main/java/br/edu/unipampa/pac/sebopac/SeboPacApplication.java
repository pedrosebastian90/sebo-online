package br.edu.unipampa.pac.sebopac;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SeboPacApplication {

	public static void main(String[] args) {
		SpringApplication.run(SeboPacApplication.class, args);
	}

}
