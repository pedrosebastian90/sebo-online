# Sebo Online

Projeto para a disciplina de Práticas em Ambientes Computacionais do curso de Engenharia de Software da Universidade Federal do Pampa (UNIPAMPA).

Sebo de livros online.

## Ferramentas e tecnologias a serem utilizadas no projeto:
- IntelliJ (IDE de Desenvolvimento)
- Java (Linguagem de Programação)
- Spring Boot (Framework Web)
- Tomcat (Servidor de Aplicação)
- MongoDB (Banco de Dados)
- GitKraken (Cliente Git)

## Requisitos Iniciais do Sistema

Abaixo o diagrama de casos de uso elicitando os requisitos iniciais do projeto:

![Diagrama de Casos de Uso](https://gitlab.com/pedrosebastian90/sebo-online/raw/master/images/Requisitos_Iniciais.png "Diagrama de Casos de Uso")